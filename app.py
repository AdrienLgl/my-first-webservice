import web
import xml.etree.ElementTree as ET

# Loading file
tree = ET.parse('temp.xml')
root = tree.getroot()

# Generating routes
urls = (
    '/([ckf])=(.*)', 'temperature'
)
app = web.application(urls, globals())

def convert(value: int, temp: str):
    if temp == 'c':
        return {'c': round(value), 'f': round((value * 1.8) - 32, 2), 'k': round(value + 273.15, 2)}
    if temp == 'f':
        return {'c': round((value-32)/1.8, 2), 'f': round(value), 'k': round(((value-32)/1.8) + 273.15, 2)}
    if temp == 'k':
        return {'c': round(value - 273.15, 2), 'f': round((((value - 273.15)*1.8) + 32), 2), 'k': round(value)}

def writeXML(temp):
    for c in root.iter('celsius'):
        c.text = str(temp['c'])
    for k in root.iter('kelvin'):
        k.text = str(temp['k'])
    for f in root.iter('fahrenheit'):
        f.text = str(temp['f'])

class temperature:
    def GET(self, name, value):
        if name == 'c':
            temp = convert(int(value), 'c')
        if name == 'f':
            temp = convert(int(value), 'f')
        if name == 'k':
            temp = convert(int(value), 'k')
        writeXML(temp)
        return ET.tostring(root)

if __name__ == "__main__":
    app.run()
